// Generated by rstantools.  Do not edit by hand.

/*
    baseqtl is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    baseqtl is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with baseqtl.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MODELS_HPP
#define MODELS_HPP
#define STAN__SERVICES__COMMAND_HPP
#include <rstan/rstaninc.hpp>
// Code generated by Stan version 2.21.0
#include <stan/model/model_header.hpp>
namespace model_GT_nb_namespace {
using std::istream;
using std::string;
using std::stringstream;
using std::vector;
using stan::io::dump;
using stan::math::lgamma;
using stan::model::prob_grad;
using namespace stan::math;
static int current_statement_begin__;
stan::io::program_reader prog_reader__() {
    stan::io::program_reader reader;
    reader.add_event(0, 0, "start", "model_GT_nb");
    reader.add_event(58, 56, "end", "model_GT_nb");
    return reader;
}
#include <stan_meta_header.hpp>
class model_GT_nb
  : public stan::model::model_base_crtp<model_GT_nb> {
private:
        int N;
        int K;
        int k;
        std::vector<int> Y;
        std::vector<int> g;
        matrix_d cov;
        vector_d aveP;
        vector_d sdP;
        vector_d mixP;
public:
    model_GT_nb(stan::io::var_context& context__,
        std::ostream* pstream__ = 0)
        : model_base_crtp(0) {
        ctor_body(context__, 0, pstream__);
    }
    model_GT_nb(stan::io::var_context& context__,
        unsigned int random_seed__,
        std::ostream* pstream__ = 0)
        : model_base_crtp(0) {
        ctor_body(context__, random_seed__, pstream__);
    }
    void ctor_body(stan::io::var_context& context__,
                   unsigned int random_seed__,
                   std::ostream* pstream__) {
        typedef double local_scalar_t__;
        boost::ecuyer1988 base_rng__ =
          stan::services::util::create_rng(random_seed__, 0);
        (void) base_rng__;  // suppress unused var warning
        current_statement_begin__ = -1;
        static const char* function__ = "model_GT_nb_namespace::model_GT_nb";
        (void) function__;  // dummy to suppress unused var warning
        size_t pos__;
        (void) pos__;  // dummy to suppress unused var warning
        std::vector<int> vals_i__;
        std::vector<double> vals_r__;
        local_scalar_t__ DUMMY_VAR__(std::numeric_limits<double>::quiet_NaN());
        (void) DUMMY_VAR__;  // suppress unused var warning
        try {
            // initialize data block variables from context__
            current_statement_begin__ = 4;
            context__.validate_dims("data initialization", "N", "int", context__.to_vec());
            N = int(0);
            vals_i__ = context__.vals_i("N");
            pos__ = 0;
            N = vals_i__[pos__++];
            check_greater_or_equal(function__, "N", N, 0);
            current_statement_begin__ = 5;
            context__.validate_dims("data initialization", "K", "int", context__.to_vec());
            K = int(0);
            vals_i__ = context__.vals_i("K");
            pos__ = 0;
            K = vals_i__[pos__++];
            check_greater_or_equal(function__, "K", K, 0);
            current_statement_begin__ = 6;
            context__.validate_dims("data initialization", "k", "int", context__.to_vec());
            k = int(0);
            vals_i__ = context__.vals_i("k");
            pos__ = 0;
            k = vals_i__[pos__++];
            check_greater_or_equal(function__, "k", k, 0);
            current_statement_begin__ = 7;
            validate_non_negative_index("Y", "N", N);
            context__.validate_dims("data initialization", "Y", "int", context__.to_vec(N));
            Y = std::vector<int>(N, int(0));
            vals_i__ = context__.vals_i("Y");
            pos__ = 0;
            size_t Y_k_0_max__ = N;
            for (size_t k_0__ = 0; k_0__ < Y_k_0_max__; ++k_0__) {
                Y[k_0__] = vals_i__[pos__++];
            }
            current_statement_begin__ = 8;
            validate_non_negative_index("g", "N", N);
            context__.validate_dims("data initialization", "g", "int", context__.to_vec(N));
            g = std::vector<int>(N, int(0));
            vals_i__ = context__.vals_i("g");
            pos__ = 0;
            size_t g_k_0_max__ = N;
            for (size_t k_0__ = 0; k_0__ < g_k_0_max__; ++k_0__) {
                g[k_0__] = vals_i__[pos__++];
            }
            current_statement_begin__ = 9;
            validate_non_negative_index("cov", "N", N);
            validate_non_negative_index("cov", "(1 + K)", (1 + K));
            context__.validate_dims("data initialization", "cov", "matrix_d", context__.to_vec(N,(1 + K)));
            cov = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>(N, (1 + K));
            vals_r__ = context__.vals_r("cov");
            pos__ = 0;
            size_t cov_j_2_max__ = (1 + K);
            size_t cov_j_1_max__ = N;
            for (size_t j_2__ = 0; j_2__ < cov_j_2_max__; ++j_2__) {
                for (size_t j_1__ = 0; j_1__ < cov_j_1_max__; ++j_1__) {
                    cov(j_1__, j_2__) = vals_r__[pos__++];
                }
            }
            current_statement_begin__ = 10;
            validate_non_negative_index("aveP", "k", k);
            context__.validate_dims("data initialization", "aveP", "vector_d", context__.to_vec(k));
            aveP = Eigen::Matrix<double, Eigen::Dynamic, 1>(k);
            vals_r__ = context__.vals_r("aveP");
            pos__ = 0;
            size_t aveP_j_1_max__ = k;
            for (size_t j_1__ = 0; j_1__ < aveP_j_1_max__; ++j_1__) {
                aveP(j_1__) = vals_r__[pos__++];
            }
            current_statement_begin__ = 11;
            validate_non_negative_index("sdP", "k", k);
            context__.validate_dims("data initialization", "sdP", "vector_d", context__.to_vec(k));
            sdP = Eigen::Matrix<double, Eigen::Dynamic, 1>(k);
            vals_r__ = context__.vals_r("sdP");
            pos__ = 0;
            size_t sdP_j_1_max__ = k;
            for (size_t j_1__ = 0; j_1__ < sdP_j_1_max__; ++j_1__) {
                sdP(j_1__) = vals_r__[pos__++];
            }
            current_statement_begin__ = 12;
            validate_non_negative_index("mixP", "k", k);
            context__.validate_dims("data initialization", "mixP", "vector_d", context__.to_vec(k));
            mixP = Eigen::Matrix<double, Eigen::Dynamic, 1>(k);
            vals_r__ = context__.vals_r("mixP");
            pos__ = 0;
            size_t mixP_j_1_max__ = k;
            for (size_t j_1__ = 0; j_1__ < mixP_j_1_max__; ++j_1__) {
                mixP(j_1__) = vals_r__[pos__++];
            }
            // initialize transformed data variables
            // execute transformed data statements
            // validate transformed data
            // validate, set parameter ranges
            num_params_r__ = 0U;
            param_ranges_i__.clear();
            current_statement_begin__ = 17;
            validate_non_negative_index("betas", "K", K);
            num_params_r__ += K;
            current_statement_begin__ = 18;
            num_params_r__ += 1;
            current_statement_begin__ = 19;
            num_params_r__ += 1;
        } catch (const std::exception& e) {
            stan::lang::rethrow_located(e, current_statement_begin__, prog_reader__());
            // Next line prevents compiler griping about no return
            throw std::runtime_error("*** IF YOU SEE THIS, PLEASE REPORT A BUG ***");
        }
    }
    ~model_GT_nb() { }
    void transform_inits(const stan::io::var_context& context__,
                         std::vector<int>& params_i__,
                         std::vector<double>& params_r__,
                         std::ostream* pstream__) const {
        typedef double local_scalar_t__;
        stan::io::writer<double> writer__(params_r__, params_i__);
        size_t pos__;
        (void) pos__; // dummy call to supress warning
        std::vector<double> vals_r__;
        std::vector<int> vals_i__;
        current_statement_begin__ = 17;
        if (!(context__.contains_r("betas")))
            stan::lang::rethrow_located(std::runtime_error(std::string("Variable betas missing")), current_statement_begin__, prog_reader__());
        vals_r__ = context__.vals_r("betas");
        pos__ = 0U;
        validate_non_negative_index("betas", "K", K);
        context__.validate_dims("parameter initialization", "betas", "vector_d", context__.to_vec(K));
        Eigen::Matrix<double, Eigen::Dynamic, 1> betas(K);
        size_t betas_j_1_max__ = K;
        for (size_t j_1__ = 0; j_1__ < betas_j_1_max__; ++j_1__) {
            betas(j_1__) = vals_r__[pos__++];
        }
        try {
            writer__.vector_unconstrain(betas);
        } catch (const std::exception& e) {
            stan::lang::rethrow_located(std::runtime_error(std::string("Error transforming variable betas: ") + e.what()), current_statement_begin__, prog_reader__());
        }
        current_statement_begin__ = 18;
        if (!(context__.contains_r("bj")))
            stan::lang::rethrow_located(std::runtime_error(std::string("Variable bj missing")), current_statement_begin__, prog_reader__());
        vals_r__ = context__.vals_r("bj");
        pos__ = 0U;
        context__.validate_dims("parameter initialization", "bj", "double", context__.to_vec());
        double bj(0);
        bj = vals_r__[pos__++];
        try {
            writer__.scalar_unconstrain(bj);
        } catch (const std::exception& e) {
            stan::lang::rethrow_located(std::runtime_error(std::string("Error transforming variable bj: ") + e.what()), current_statement_begin__, prog_reader__());
        }
        current_statement_begin__ = 19;
        if (!(context__.contains_r("phi")))
            stan::lang::rethrow_located(std::runtime_error(std::string("Variable phi missing")), current_statement_begin__, prog_reader__());
        vals_r__ = context__.vals_r("phi");
        pos__ = 0U;
        context__.validate_dims("parameter initialization", "phi", "double", context__.to_vec());
        double phi(0);
        phi = vals_r__[pos__++];
        try {
            writer__.scalar_lb_unconstrain(0, phi);
        } catch (const std::exception& e) {
            stan::lang::rethrow_located(std::runtime_error(std::string("Error transforming variable phi: ") + e.what()), current_statement_begin__, prog_reader__());
        }
        params_r__ = writer__.data_r();
        params_i__ = writer__.data_i();
    }
    void transform_inits(const stan::io::var_context& context,
                         Eigen::Matrix<double, Eigen::Dynamic, 1>& params_r,
                         std::ostream* pstream__) const {
      std::vector<double> params_r_vec;
      std::vector<int> params_i_vec;
      transform_inits(context, params_i_vec, params_r_vec, pstream__);
      params_r.resize(params_r_vec.size());
      for (int i = 0; i < params_r.size(); ++i)
        params_r(i) = params_r_vec[i];
    }
    template <bool propto__, bool jacobian__, typename T__>
    T__ log_prob(std::vector<T__>& params_r__,
                 std::vector<int>& params_i__,
                 std::ostream* pstream__ = 0) const {
        typedef T__ local_scalar_t__;
        local_scalar_t__ DUMMY_VAR__(std::numeric_limits<double>::quiet_NaN());
        (void) DUMMY_VAR__;  // dummy to suppress unused var warning
        T__ lp__(0.0);
        stan::math::accumulator<T__> lp_accum__;
        try {
            stan::io::reader<local_scalar_t__> in__(params_r__, params_i__);
            // model parameters
            current_statement_begin__ = 17;
            Eigen::Matrix<local_scalar_t__, Eigen::Dynamic, 1> betas;
            (void) betas;  // dummy to suppress unused var warning
            if (jacobian__)
                betas = in__.vector_constrain(K, lp__);
            else
                betas = in__.vector_constrain(K);
            current_statement_begin__ = 18;
            local_scalar_t__ bj;
            (void) bj;  // dummy to suppress unused var warning
            if (jacobian__)
                bj = in__.scalar_constrain(lp__);
            else
                bj = in__.scalar_constrain();
            current_statement_begin__ = 19;
            local_scalar_t__ phi;
            (void) phi;  // dummy to suppress unused var warning
            if (jacobian__)
                phi = in__.scalar_lb_constrain(0, lp__);
            else
                phi = in__.scalar_lb_constrain(0);
            // model body
            {
            current_statement_begin__ = 24;
            validate_non_negative_index("lmu", "N", N);
            Eigen::Matrix<local_scalar_t__, Eigen::Dynamic, 1> lmu(N);
            stan::math::initialize(lmu, DUMMY_VAR__);
            stan::math::fill(lmu, DUMMY_VAR__);
            current_statement_begin__ = 25;
            local_scalar_t__ ebj(DUMMY_VAR__);
            (void) ebj;  // dummy to suppress unused var warning
            stan::math::initialize(ebj, DUMMY_VAR__);
            stan::math::fill(ebj, DUMMY_VAR__);
            current_statement_begin__ = 26;
            validate_non_negative_index("lps", "k", k);
            Eigen::Matrix<local_scalar_t__, Eigen::Dynamic, 1> lps(k);
            stan::math::initialize(lps, DUMMY_VAR__);
            stan::math::fill(lps, DUMMY_VAR__);
            current_statement_begin__ = 30;
            lp_accum__.add(gamma_log<propto__>(phi, 1, 0.01));
            current_statement_begin__ = 32;
            lp_accum__.add(normal_log<propto__>(get_base1(betas, 1, "betas", 1), 6, 4));
            current_statement_begin__ = 33;
            for (int i = 2; i <= K; ++i) {
                current_statement_begin__ = 34;
                lp_accum__.add(cauchy_log<propto__>(get_base1(betas, i, "betas", 1), 0, 2.5));
            }
            current_statement_begin__ = 38;
            for (int i = 1; i <= k; ++i) {
                current_statement_begin__ = 39;
                stan::model::assign(lps, 
                            stan::model::cons_list(stan::model::index_uni(i), stan::model::nil_index_list()), 
                            (normal_log(bj, get_base1(aveP, i, "aveP", 1), get_base1(sdP, i, "sdP", 1)) + get_base1(mixP, i, "mixP", 1)), 
                            "assigning variable lps");
            }
            current_statement_begin__ = 41;
            lp_accum__.add(log_sum_exp(lps));
            current_statement_begin__ = 45;
            stan::math::assign(ebj, stan::math::exp(bj));
            current_statement_begin__ = 46;
            stan::math::assign(lmu, multiply(stan::model::rvalue(cov, stan::model::cons_list(stan::model::index_omni(), stan::model::cons_list(stan::model::index_min_max(2, cols(cov)), stan::model::nil_index_list())), "cov"), betas));
            current_statement_begin__ = 47;
            for (int i = 1; i <= N; ++i) {
                current_statement_begin__ = 48;
                stan::model::assign(lmu, 
                            stan::model::cons_list(stan::model::index_uni(i), stan::model::nil_index_list()), 
                            (logical_eq(stan::math::fabs(get_base1(g, i, "g", 1)), 1) ? stan::math::promote_scalar<local_scalar_t__>(((get_base1(lmu, i, "lmu", 1) + stan::math::log1p(ebj)) - stan::math::log(2))) : stan::math::promote_scalar<local_scalar_t__>(get_base1(lmu, i, "lmu", 1)) ), 
                            "assigning variable lmu");
                current_statement_begin__ = 49;
                stan::model::assign(lmu, 
                            stan::model::cons_list(stan::model::index_uni(i), stan::model::nil_index_list()), 
                            (logical_eq(get_base1(g, i, "g", 1), 2) ? stan::math::promote_scalar<local_scalar_t__>((get_base1(lmu, i, "lmu", 1) + bj)) : stan::math::promote_scalar<local_scalar_t__>(get_base1(lmu, i, "lmu", 1)) ), 
                            "assigning variable lmu");
                current_statement_begin__ = 50;
                lp_accum__.add(neg_binomial_2_log(get_base1(Y, i, "Y", 1), stan::math::exp(get_base1(lmu, i, "lmu", 1)), phi));
            }
            }
        } catch (const std::exception& e) {
            stan::lang::rethrow_located(e, current_statement_begin__, prog_reader__());
            // Next line prevents compiler griping about no return
            throw std::runtime_error("*** IF YOU SEE THIS, PLEASE REPORT A BUG ***");
        }
        lp_accum__.add(lp__);
        return lp_accum__.sum();
    } // log_prob()
    template <bool propto, bool jacobian, typename T_>
    T_ log_prob(Eigen::Matrix<T_,Eigen::Dynamic,1>& params_r,
               std::ostream* pstream = 0) const {
      std::vector<T_> vec_params_r;
      vec_params_r.reserve(params_r.size());
      for (int i = 0; i < params_r.size(); ++i)
        vec_params_r.push_back(params_r(i));
      std::vector<int> vec_params_i;
      return log_prob<propto,jacobian,T_>(vec_params_r, vec_params_i, pstream);
    }
    void get_param_names(std::vector<std::string>& names__) const {
        names__.resize(0);
        names__.push_back("betas");
        names__.push_back("bj");
        names__.push_back("phi");
    }
    void get_dims(std::vector<std::vector<size_t> >& dimss__) const {
        dimss__.resize(0);
        std::vector<size_t> dims__;
        dims__.resize(0);
        dims__.push_back(K);
        dimss__.push_back(dims__);
        dims__.resize(0);
        dimss__.push_back(dims__);
        dims__.resize(0);
        dimss__.push_back(dims__);
    }
    template <typename RNG>
    void write_array(RNG& base_rng__,
                     std::vector<double>& params_r__,
                     std::vector<int>& params_i__,
                     std::vector<double>& vars__,
                     bool include_tparams__ = true,
                     bool include_gqs__ = true,
                     std::ostream* pstream__ = 0) const {
        typedef double local_scalar_t__;
        vars__.resize(0);
        stan::io::reader<local_scalar_t__> in__(params_r__, params_i__);
        static const char* function__ = "model_GT_nb_namespace::write_array";
        (void) function__;  // dummy to suppress unused var warning
        // read-transform, write parameters
        Eigen::Matrix<double, Eigen::Dynamic, 1> betas = in__.vector_constrain(K);
        size_t betas_j_1_max__ = K;
        for (size_t j_1__ = 0; j_1__ < betas_j_1_max__; ++j_1__) {
            vars__.push_back(betas(j_1__));
        }
        double bj = in__.scalar_constrain();
        vars__.push_back(bj);
        double phi = in__.scalar_lb_constrain(0);
        vars__.push_back(phi);
        double lp__ = 0.0;
        (void) lp__;  // dummy to suppress unused var warning
        stan::math::accumulator<double> lp_accum__;
        local_scalar_t__ DUMMY_VAR__(std::numeric_limits<double>::quiet_NaN());
        (void) DUMMY_VAR__;  // suppress unused var warning
        if (!include_tparams__ && !include_gqs__) return;
        try {
            if (!include_gqs__ && !include_tparams__) return;
            if (!include_gqs__) return;
        } catch (const std::exception& e) {
            stan::lang::rethrow_located(e, current_statement_begin__, prog_reader__());
            // Next line prevents compiler griping about no return
            throw std::runtime_error("*** IF YOU SEE THIS, PLEASE REPORT A BUG ***");
        }
    }
    template <typename RNG>
    void write_array(RNG& base_rng,
                     Eigen::Matrix<double,Eigen::Dynamic,1>& params_r,
                     Eigen::Matrix<double,Eigen::Dynamic,1>& vars,
                     bool include_tparams = true,
                     bool include_gqs = true,
                     std::ostream* pstream = 0) const {
      std::vector<double> params_r_vec(params_r.size());
      for (int i = 0; i < params_r.size(); ++i)
        params_r_vec[i] = params_r(i);
      std::vector<double> vars_vec;
      std::vector<int> params_i_vec;
      write_array(base_rng, params_r_vec, params_i_vec, vars_vec, include_tparams, include_gqs, pstream);
      vars.resize(vars_vec.size());
      for (int i = 0; i < vars.size(); ++i)
        vars(i) = vars_vec[i];
    }
    std::string model_name() const {
        return "model_GT_nb";
    }
    void constrained_param_names(std::vector<std::string>& param_names__,
                                 bool include_tparams__ = true,
                                 bool include_gqs__ = true) const {
        std::stringstream param_name_stream__;
        size_t betas_j_1_max__ = K;
        for (size_t j_1__ = 0; j_1__ < betas_j_1_max__; ++j_1__) {
            param_name_stream__.str(std::string());
            param_name_stream__ << "betas" << '.' << j_1__ + 1;
            param_names__.push_back(param_name_stream__.str());
        }
        param_name_stream__.str(std::string());
        param_name_stream__ << "bj";
        param_names__.push_back(param_name_stream__.str());
        param_name_stream__.str(std::string());
        param_name_stream__ << "phi";
        param_names__.push_back(param_name_stream__.str());
        if (!include_gqs__ && !include_tparams__) return;
        if (include_tparams__) {
        }
        if (!include_gqs__) return;
    }
    void unconstrained_param_names(std::vector<std::string>& param_names__,
                                   bool include_tparams__ = true,
                                   bool include_gqs__ = true) const {
        std::stringstream param_name_stream__;
        size_t betas_j_1_max__ = K;
        for (size_t j_1__ = 0; j_1__ < betas_j_1_max__; ++j_1__) {
            param_name_stream__.str(std::string());
            param_name_stream__ << "betas" << '.' << j_1__ + 1;
            param_names__.push_back(param_name_stream__.str());
        }
        param_name_stream__.str(std::string());
        param_name_stream__ << "bj";
        param_names__.push_back(param_name_stream__.str());
        param_name_stream__.str(std::string());
        param_name_stream__ << "phi";
        param_names__.push_back(param_name_stream__.str());
        if (!include_gqs__ && !include_tparams__) return;
        if (include_tparams__) {
        }
        if (!include_gqs__) return;
    }
}; // model
}  // namespace
typedef model_GT_nb_namespace::model_GT_nb stan_model;
#ifndef USING_R
stan::model::model_base& new_model(
        stan::io::var_context& data_context,
        unsigned int seed,
        std::ostream* msg_stream) {
  stan_model* m = new stan_model(data_context, seed, msg_stream);
  return *m;
}
#endif
#endif
